
//In correlation mode, we need to delay the stop (stop = secondCh,secondMod)
//The start is always the same reference and should not be used as stop
//The time spectra are in nanoseconds (RL update Oct 2024)



void correlations() {
	
  
  for (j=0; j<corr_format; j++) {

		k=0;
		i=1;
        int energy_overflow = 60000;
        int energy_threshold = 10;
        int dt;
		while (k<iData)
		{
			l=1;
			while ( DataArray[k].chnum == firstCh[j] && 
			        DataArray[k].modnum == firstMod[j] && 
			        k+l<iData && 
			        DataArray[k].energy > energy_threshold && 
			        DataArray[k].energy < energy_overflow)
			        {
						dt = (int)(DataArray[k+l].time + DataArray[k+l].cfdTime - DataArray[k].time - DataArray[k].cfdTime);
						if (DataArray[k+l].chnum  == secondCh[j] &&
						 DataArray[k+l].modnum == secondMod[j] && 
						 DataArray[k+l].energy > energy_threshold && 
						 DataArray[k+l].energy < energy_overflow && 
						 dt < RANGE  )
						 {
							 corr_hist[j][dt]++;
							 }
						if ( DataArray[k+l].chnum  == firstCh[j] && DataArray[k+l].modnum == firstMod[j] ) k=k+l-1;
						l++;
						}
			k++;
		}
		
  }
		
		
	printf("-------------------\n");
	for (j=0; j<corr_format; j++) {
	
		
	int ymax=0, xmax=0, dx=0;
	for (z=0; z<RANGE; z++)  if (corr_hist[j][z] > ymax) { ymax = corr_hist[j][z]; xmax=z; dx=CORR_DELAY-xmax; }
	
	printf("R%d_M%dC%d-M%dC%d.txt|a:64\t\t Ymax = %d\t Xmax = %d\t dX = %d \n", runnumber, firstMod[j], firstCh[j], secondMod[j], secondCh[j],  ymax, xmax, dx);
	
	}
	printf("-------------------\n");
	
	
	
	
	
	
}
